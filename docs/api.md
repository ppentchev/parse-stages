<!--
SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
SPDX-License-Identifier: BSD-2-Clause
-->

# API Reference

## The tagged object classes

::: parse_stages.Tagged

::: parse_stages.TaggedFrozen

## The evaluated boolean expression

::: parse_stages.BoolExpr

::: parse_stages.OrExpr

::: parse_stages.AndExpr

::: parse_stages.NotExpr

## The object's tags and keywords

::: parse_stages.TagExpr

::: parse_stages.KeywordExpr

## The helper functions

::: parse_stages.EMPTY_SET_SPECS

::: parse_stages.parse_spec

::: parse_stages.parse_stage_ids
