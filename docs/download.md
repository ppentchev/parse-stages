<!--
SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
SPDX-License-Identifier: BSD-2-Clause
-->

# Download

These are the released versions of [parse-stages](index.md) available for download.

## [0.1.9] - 2024-08-09

### Source tarball

- [parse_stages-0.1.9.tar.gz](https://devel.ringlet.net/files/devel/parse-stages/parse_stages-0.1.9.tar.gz)
  (with [a PGP signature](https://devel.ringlet.net/files/devel/parse-stages/parse_stages-0.1.9.tar.gz.asc))

### Python wheel

- [parse_stages-0.1.9-py3-none-any.whl](https://devel.ringlet.net/files/devel/parse-stages/parse_stages-0.1.9-py3-none-any.whl)
  (with [a PGP signature](https://devel.ringlet.net/files/devel/parse-stages/parse_stages-0.1.9-py3-none-any.whl.asc))

## [0.1.8] - 2024-02-08

### Source tarball

- [parse_stages-0.1.8.tar.gz](https://devel.ringlet.net/files/devel/parse-stages/parse_stages-0.1.8.tar.gz)
  (with [a PGP signature](https://devel.ringlet.net/files/devel/parse-stages/parse_stages-0.1.8.tar.gz.asc))

### Python wheel

- [parse_stages-0.1.8-py3-none-any.whl](https://devel.ringlet.net/files/devel/parse-stages/parse_stages-0.1.8-py3-none-any.whl)
  (with [a PGP signature](https://devel.ringlet.net/files/devel/parse-stages/parse_stages-0.1.8-py3-none-any.whl.asc))

## [0.1.7] - 2023-11-08

### Source tarball

- [parse_stages-0.1.7.tar.gz](https://devel.ringlet.net/files/devel/parse-stages/parse_stages-0.1.7.tar.gz)
  (with [a PGP signature](https://devel.ringlet.net/files/devel/parse-stages/parse_stages-0.1.7.tar.gz.asc))

### Python wheel

- [parse_stages-0.1.7-py3-none-any.whl](https://devel.ringlet.net/files/devel/parse-stages/parse_stages-0.1.7-py3-none-any.whl)
  (with [a PGP signature](https://devel.ringlet.net/files/devel/parse-stages/parse_stages-0.1.7-py3-none-any.whl.asc))

[0.1.9]: https://gitlab.com/ppentchev/parse-stages/-/tags/release%2F0.1.9
[0.1.8]: https://gitlab.com/ppentchev/parse-stages/-/tags/release%2F0.1.8
[0.1.7]: https://gitlab.com/ppentchev/parse-stages/-/tags/release%2F0.1.7
